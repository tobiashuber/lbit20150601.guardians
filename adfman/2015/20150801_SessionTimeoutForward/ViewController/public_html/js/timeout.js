/**
 * function being called from ADF application, when the page is being loaded the first time
 */
function onLoad() {
  calcOffset();
  checkSession();
};

/**
 * calculate the offset between server time and client time
 */
function calcOffset() {
  var serverTime = getCookie('serverTime');
  serverTime = serverTime == null ? null : Math.abs(serverTime);
  var clientTimeOffset = (new Date()).getTime() - serverTime;
  setCookie('clientTimeOffset', clientTimeOffset);
}

/**
 * Check periodically each second, if the session has timed out
 */
function checkSession() {
  var outputText = AdfPage.PAGE.findComponentByAbsoluteId("ot3");
    
  var sessionExpiry = Math.abs(getCookie('sessionExpiry'));
  var timeOffset = Math.abs(getCookie('clientTimeOffset'));
  var localTime = (new Date()).getTime();
  if (localTime - timeOffset > (sessionExpiry + 5000)) {
    // 5 extra seconds to make sure
    window.location = "/SessionTimeoutForward-ViewController-context-root/faces/sessionTimeout.jspx";
  } else if (localTime - timeOffset > sessionExpiry) {
    outputText.setValue('Session timed out. Forward in five seconds !!!');  
    setTimeout('checkSession()', 1000);
  }
  else {
    // calculate remaining time in seconds to show it to the client
    var remainingTime = Math.round((sessionExpiry - (localTime - timeOffset)) / 1000);
    outputText.setValue('remainingTime: ' + remainingTime + ', timeOffset: ' + timeOffset + 'ms');
    setTimeout('checkSession()', 1000);
  }
}

/**
 * read cookie from browser
 * @param key
 */
function getCookie(key) {
  var result;
  return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? (result[1]) : null;
}

/**
 * write cookie to browser
 * @param key
 * @param value
 */
function setCookie(key, value) {
  var mycookie = key + '=' + value;
  document.cookie = mycookie;
}